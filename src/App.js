import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import '@rentalkit/core/modules';

class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Welcome to React</h2>
        </div>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
          <button is="rental-button">HelloBtn</button>
          <br/>
          <div className="page">
              <section>
                  <h1>Main Heading</h1>
                  <sitemate-modal>
                      <h3 slot="header">Main Confirmation Message</h3>
                      <p slot="message">Are you sure you want to continue?</p>
                  </sitemate-modal>
              </section>
          </div>
      </div>
    );
  }
}

export default App;
